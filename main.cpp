#include <GL/freeglut.h>
#include <iostream>
#include "Point.h"
#include "Rectangle.h"
#include "Button.h"
#include "TextureButton.h"

using namespace std;

TextureButton pencilButton("assets/pencil.png", -1.0f, 1.0f, 0.2f, 0.2f);
TextureButton eraserButton("assets/eraser.png", -1.0f, 0.6f, 0.2f, 0.2f);
TextureButton mouseButton("assets/mouse.png", -1.0f, 0.2f, 0.2f, 0.2f);
TextureButton saveButton("assets/save.png", -1.0f, -0.2f, 0.2f, 0.2f);

Rectangle verticalToolbar(-1.0f, 1.0f, 0.2f, 2.0f, Color(0.7f, 0.7f, 0.7f));
Rectangle horizontalToolbar(-0.8f, -0.8f, 1.8f, 0.2f, Color(0.7f, 0.7f, 0.7f));
Rectangle canvas(-0.8f, 1.0f, 1.8f, 1.8f, Color(0.9f, 0.9f, 0.9f));

Rectangle redButton(-0.8f, -0.8f, 0.4f, 0.2f, Color(1.0f, 0.0f, 0.0f));
Rectangle greenButton(-0.3f, -0.8f, 0.4f, 0.2f, Color(0.0f, 1.0f, 0.0f));
Rectangle blueButton(0.2f, -0.8f, 0.4f, 0.2f, Color(0.0f, 0.0f, 1.0f));

Button clearButton("Clear", 0.7f, -0.8f);

const int maxPoints = 10000;
Point points[maxPoints];
int pointsCount = 0;

// Window width and height
int width = 400;
int height = 400;
Color brushColor;

// Convert window coordinates to Cartesian coordinates
void windowToScene(float& x, float& y) {
    x = (2.0f * (x / float(width))) - 1.0f;
    y = 1.0f - (2.0f * (y / float(height)));
}

void drawScene(){
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Objects to be drawn go here
    glEnable(GL_POINT_SMOOTH);
    glPointSize(15.0f);

    canvas.draw();

    for (int i = 0; i < pointsCount; i++) {
        points[i].draw();
    }

    verticalToolbar.draw();
    horizontalToolbar.draw();

    redButton.draw();
    greenButton.draw();
    blueButton.draw();
    clearButton.draw();

    pencilButton.draw();
    eraserButton.draw();
    mouseButton.draw();
    saveButton.draw();

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
    /*
        button: 0 -> left mouse button
                2 -> right mouse button
        
        state:  0 -> mouse click
                1 -> mouse release
        
        x, y:   mouse location in window relative coordinates
    */

    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (button == 0 && state == 0) {
        if (verticalToolbar.isClicked(mx, my)) {
            cout << "Vertical toolbar clicked" << endl;
        } else if (horizontalToolbar.isClicked(mx, my)) {
            if (redButton.isClicked(mx, my)) {
                cout << "Selected Color: Red" << endl;
                brushColor.setRed();
            } else if (greenButton.isClicked(mx, my)) {
                cout << "Selected Color: Green" << endl;
                brushColor.setGreen();
            } else if (blueButton.isClicked(mx, my)) {
                cout << "Selected Color: Blue" << endl;
                brushColor.setBlue();
            } else if (clearButton.isClicked(mx, my)) {
                clearButton.press();
                pointsCount = 0;
                cout << "Clear screen" << endl;
            }
        } else if (canvas.isClicked(mx, my)) {
            if (pointsCount < maxPoints) {
                points[pointsCount] = Point(mx, my, brushColor);
                pointsCount++;
            }
        }
    } else if (button == 0 && state == 1) {
        clearButton.release();
    }

    glutPostRedisplay();
}

void motion(int x, int y) {
    /*
        x, y:   mouse location in window relative coordinates
    */
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (canvas.isClicked(mx, my)) {
        if (pointsCount < maxPoints) {
            points[pointsCount] = Point(mx, my, brushColor);
            pointsCount++;
        }
    }


    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
    /*
        key:    ASCII character of the keyboard key that was pressed
        x, y:   mouse location in window relative coordinates
    */
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    // Set the Mouse Function
    glutMouseFunc(mouse);

    // Set the Motion Function
    glutMotionFunc(motion);

    // Set the Keyboard Funcion
    glutKeyboardFunc(keyboard);

    // Load textures
    pencilButton.loadTexture();
    eraserButton.loadTexture();
    mouseButton.loadTexture();
    saveButton.loadTexture();

    // Run the program
    glutMainLoop();


    return 0;
}